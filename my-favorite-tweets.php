<?php
    session_start();

    if (!isset($_SESSION['favorite'])) {
        $_SESSION['favorite'] = [];
    }

    if (isset($_GET['remove'])) {
        $_SESSION['favorite']=[];
    }
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://abs.twimg.com/favicons/twitter.2.ico" type="image/x-icon">
    <title>My favorite Tweets</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
        // VARIABLES

        // Récupérer le contenu du fichier tweets.json
        $json_str = file_get_contents('tweets.json');

        // Décoder le contenu JSON en UTF-8 pour avoir le même encodage avec l'affichage
        $json_str_utf8 = utf8_decode($json_str);
        // Décoder le contenu JSON en tant que tableau associatif
        $tweets = json_decode($json_str_utf8, true)['data'];

        // Déterminer le nombre de tweets
        $num_tweets = count($tweets);
    ?>

    <!-- Menu de la page -->
    <nav class="navbar navbar-expand-lg bg-dark fixed-top navbar-dark menu">
        <div style="margin-left: 1em;">
            <a class="navbar-brand" href="index.php"><img src="images/twitter_white.svg" alt="Twitter Project" height="40"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="margin-right: 1em;">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="menu__item"><a href="index.php"><img src="images/explore.svg" alt="Explore" height="30"> Explore</a></li>
                <li class="menu__item"><a href="sentiment-analysis.php"><img src="images/add_chart.svg" alt="Sentiment analysis" height="30"> Sentiment analysis</a></li>
                <li class="menu__item"><a href="additional-infos.php"><img src="images/infos.svg" alt="Additional info" height="30"> Additional info</a></li>
                <li class="menu__item"><a href="my-favorite-tweets.php" class="active"><img src="images/favorite-white.svg" alt="My favorite Tweets" height="30"> My favorite Tweets</a></li>
                <li class="menu__item"><a href="https://twitter.com" target="_blank"><img src="images/logo-white.png" alt="Twitter" height="30"> Official Twitter</a></li>
                <li class="menu__search" style="margin-left: 5em;margin-right: 1em;">
                    <input type="search" name="search" id="search" class="form-control" <?php if(isset($_GET['search'])) {
                        echo 'value="' . $_GET['search'] . '"';
                    }?> placeholder="Search keyword..." maxlength="50">
                    <button class="btn btn-light" id="btnSearch"><img src="images/search.svg" height="25" alt="Search" title="Search"></button>
                </li>
            </ul>
        </div>
    </nav>

    <div class="infos">
        <div class="card bg-dark text-white">
            <div class="card-body">
                <div class="infos__header">
                    <h5 class="card-title mb-5">My favorite Tweets</h5>
                    <img src="images/delete.svg" height="50" alt="Remove all favorites" title="Remove all favorites" onclick="window.location.href = 'my-favorite-tweets.php?remove='">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="card-text"><img src="images/tweet.svg" alt="Number of tweets favorited"> Number of tweets favorited:
                            <p><strong class="card-text value"><?php echo (isset($_SESSION['favorite'])) ? count($_SESSION['favorite']) : ''; ?></strong></p>
                        </p>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="col-md-6">
                        <p class="card-text"><img src="images/date.svg" alt="Date of last favorite added"> Date of last favorite added:
                            <p><strong class="card-text value">
                                <?php
                                    $info = 'Launch Sentiment analysis of my favorite Tweets';
                                    $disabled = '';
                                    $onclick = 'onclick="window.location.href = \'sentiment-analysis.php?favorite=\'"';
                                    if (isset($_SESSION['favorite']) && count($_SESSION['favorite']) === 0) {
                                        echo '-';
                                        $info = 'Feature not available without Tweets.';
                                        $disabled = 'style="background: none;cursor: auto;"';
                                        $onclick = '';
                                    }
                                    else if (isset($_SESSION['date_favorite'])) {
                                        echo $_SESSION['date_favorite'];
                                    }
                                    else {
                                        echo '-';
                                    }
                                ?>
                            </strong></p>
                        </p>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="index.php">To explore or add tweets to favorites!</a>
                    <img src="images/add_chart.svg" <?php echo $disabled; ?> alt="<?php echo $info; ?>" title="<?php echo $info; ?>" <?php echo $onclick; ?>>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Script JS gérant la recherche des tweets avec un mot clé -->
    <?php
        echo '<script defer>
            var btnSearch = document.getElementById(\'btnSearch\');
            var search = document.getElementById(\'search\');

            btnSearch.addEventListener(\'click\', function() {
                if (search.value != \'\') {
                    var value = search.value;
                    value = value.toLowerCase();
                    value = value.replace(/ /g,\'\');
                    window.location.href = \'index.php?search=\' + value;
                }
            });

            search.addEventListener("keydown", function(event) {
                if(event.keyCode === 13) {
                    if (search.value != \'\') {
                        var value = search.value;
                        value = value.toLowerCase();
                        value = value.replace(/ /g,\'\');
                        window.location.href = \'index.php?search=\' + value;
                    }
                }
            });
        </script>';
    ?>

    <br>
    <br>
    
    <footer class="bg-dark text-light text-center py-3 mt-3">
        <div class="container">
            <p><a href="index.php"><img src="images/twitter_white.svg" height="30" alt="Twitter" title="Twitter"></a> <a href="index.php">Twitter Project</a> - Sentiment Analysis of Tweets</p>
            <p>Designed and Developed by <a href="mailto:antho.coulon22@gmail.com">Anthony Coulon</a> & <a href="mailto:titouan.comtet@gmail.com">Titouan Comtet</a></p>
            <p>Hosted by <a href="https://infinityfree.net/" target="_blank">InfinityFree <img src="images/infinityfree.png" height="50" alt="InfinityFree" title="InfinityFree"></a></p>
            <p>Tweets retrieve on <a href="https://twitter.com" target="_blank">Twitter</a> <a href="https://twitter.com" target="_blank"><img src="images/twitter_white.svg" height="30" alt="Twitter" title="Twitter"></a></p>
        </div>
    </footer>
</body>

</html>