<?php
    session_start();
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://abs.twimg.com/favicons/twitter.2.ico" type="image/x-icon">
    <?php
        $msg = 'this Tweet';
        if (isset($_GET['myfav'])) {
            $msg = 'my favorite Tweet';
        }
        $title = 'of all Tweets';
        if (isset($_GET['favorite'])) {
            $title = 'of my favorite Tweets';
        }
        else if (isset($_GET['id'])) {
            $title = 'of ' . $msg;
        }
    ?>
    <title>Sentiment analysis <?php echo $title; ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">

    <link rel="stylesheet" href="https://pyscript.net/latest/pyscript.css" />
    <script defer src="https://pyscript.net/latest/pyscript.js"></script>
    <py-config>
        packages = ["textblob", "textblob_fr", "matplotlib"]
    </py-config>
</head>

<body>
    <!-- Menu de la page -->
    <nav class="navbar navbar-expand-lg bg-dark fixed-top navbar-dark menu">
        <div style="margin-left: 1em;">
            <a class="navbar-brand" href="index.php"><img src="images/twitter_white.svg" alt="Twitter Project" height="40"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="margin-right: 1em;">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="menu__item"><a href="index.php"><img src="images/explore.svg" alt="Explore" height="30"> Explore</a></li>
                <li class="menu__item"><a href="sentiment-analysis.php" class="active"><img src="images/add_chart.svg" alt="Sentiment analysis" height="30"> Sentiment analysis</a></li>
                <li class="menu__item"><a href="additional-infos.php"><img src="images/infos.svg" alt="Additional info" height="30"> Additional info</a></li>
                <li class="menu__item"><a href="my-favorite-tweets.php"><img src="images/favorite-white.svg" alt="My favorite Tweets" height="30"> My favorite Tweets</a></li>
                <li class="menu__item"><a href="https://twitter.com" target="_blank"><img src="images/logo-white.png" alt="Twitter" height="30"> Official Twitter</a></li>
                <li class="menu__search" style="margin-left: 5em;margin-right: 1em;">
                    <input type="search" name="search" id="search" class="form-control" <?php if(isset($_GET['search'])) {
                        echo 'value="' . $_GET['search'] . '"';
                    }?> placeholder="Search keyword..." maxlength="50">
                    <button class="btn btn-light" id="btnSearch"><img src="images/search.svg" height="25" alt="Search" title="Search"></button>
                </li>
            </ul>
        </div>
    </nav>

    <?php
        // VARIABLES

        // Récupérer le contenu du fichier tweets.json
        $json_str = file_get_contents('tweets.json');

        // Décoder le contenu JSON en UTF-8 pour avoir le même encodage avec l'affichage
        $json_str_utf8 = utf8_decode($json_str);
        // Décoder le contenu JSON en tant que tableau associatif
        $tweets = json_decode($json_str_utf8, true)['data'];
        
        if (isset($_GET['id'])) {
            foreach ($tweets as $key => $value) {
                if ($value['id'] === $_GET['id']) {
                    $text = $value['text'];
                }
            }
        }
    ?>

    <?php if (isset($_GET['id'])): ?>
        <section class="pyscript">
            <div id="analysis_tweet_id"></div>
            <?php
                echo '<py-script>
                    from textblob import Blobber
                    from textblob_fr import PatternTagger, PatternAnalyzer
                    import matplotlib.pyplot as plt

                    tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
                    blob = tb(u"'.str_replace('"', '', preg_replace('/\s+/', ' ', $text)).'")

                    s1 = blob.sentiment[0]
                    s2 = blob.sentiment[1]

                    p = Element("polarity")
                    p.write(s1)
                    s = Element("subjectivity")
                    s.write(s2)

                    # Changement du fond de couleur de la figure
                    plt.style.use(\'dark_background\')

                    # Crée une figure et un axe
                    fig, ax = plt.subplots()

                    # Ajout du titre
                    plt.title("Sentiment analysis of '.$msg.'")

                    # Fixe les limites de l\'axe des ordonnées de -1 à 1
                    ax.set_ylim(-1, 1)

                    # Trace deux lignes horizontales à y1=s1 et y2=s2
                    ax.axhline(y=s1, color=\'r\', linestyle=\'-\', label=\'Polarity : \'+str(s1))
                    ax.axhline(y=s2, color=\'g\', linestyle=\'-\', label=\'Subjectivity : \'+str(s2))

                    # Ajoute la légende
                    ax.legend()

                    analysis_tweet_id = Element("analysis_tweet_id")
                    analysis_tweet_id.write(fig)
                </py-script>';

                echo '<script defer>
                    function analysePolarity(polarity) {
                        if (polarity < 0) {
                        return \'Negative\';
                        } else if (polarity > 0) {
                        return \'Positive\';
                        } else {
                        return \'Neutral\';
                        }
                    }

                    function analyseSubjectivity(subjectivity) {
                        if (subjectivity > 0.5) {
                        return \'Subjective\';
                        } else if (subjectivity >= 0.3) {
                        return \'Neutral\';
                        } else {
                        return \'Not Subjective\';
                        }
                    }

                    setTimeout(function() {
                        var p = document.getElementById(\'polarity\');
                        var s = document.getElementById(\'subjectivity\');
                        p.textContent = analysePolarity(p.textContent) + \' (\'+p.textContent+\')\';
                        s.textContent = analyseSubjectivity(s.textContent) + \' (\'+s.textContent+\')\';
                    }, 10000);
                </script>';
            ?>
            <p><?php echo 'Text of Tweets : ' . $text ?></p>
            <div class="info-analysis">
                <div class="card bg-dark text-white" style="width: 70em;display: flex;justify-content: center;">
                    <div class="card-body">
                        <h5 class="card-title mb-5">Summary of Information of <?php echo $msg; ?></h5>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="card-text"><img src="images/polarity.svg" alt="The Polarity"> The Polarity:
                                    <p>
                                        <strong class="card-text">Sentiment analysis is
                                        <span id="polarity">...</span></strong>
                                    </p>
                                </p>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-6">
                                <p class="card-text"><img src="images/subjectivity.svg" alt="The Subjectivity"> The Subjectivity:
                                    <p>
                                        <strong class="card-text">Sentiment analysis is
                                        <span id="subjectivity">...</span></strong></p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php elseif (isset($_GET['favorite'])): ?>
        <?php if (count($_SESSION['favorite']) == 1): ?>
            <?php
                foreach ($_SESSION['favorite'] as $key => $value) {
                    $fav = $value;
                }
                header('Location: sentiment-analysis.php?id=' . $fav . '&myfav=');
            ?>
        <?php else: ?>
            <section class="pyscript">
                <h3 class="text-center">Polarity of my favorite Tweets</h3>
                <div id="polarity_all_tweets">...</div>
            </section>
            <section class="pyscript">
                <h3 class="text-center">Subjectivity of my favorite Tweets</h3>
                <div id="subjectivity_all_tweets">...</div>
            </section>

            <div class="info-analysis">
                <div class="card bg-dark text-white" style="width: 70em;">
                    <div class="card-body">
                        <h5 class="card-title mb-5">Summary of Information of my favorite Tweets</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="card-text"><img src="images/polarity.svg" alt="The average of Polarity"> The average of Polarity:
                                    <p>
                                        <strong class="card-text">Sentiment analysis is
                                        <span id="polarity">...</span></strong>
                                    </p>
                                </p>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-6">
                                <p class="card-text"><img src="images/subjectivity.svg" alt="The average of Subjectivity"> The average of Subjectivity:
                                    <p>
                                        <strong class="card-text">Sentiment analysis is
                                        <span id="subjectivity">...</span></strong></p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
                echo '<script defer>
                    function analysePolarity(polarity) {
                        if (polarity < 0) {
                        return \'Negative\';
                        } else if (polarity > 0) {
                        return \'Positive\';
                        } else {
                        return \'Neutral\';
                        }
                    }

                    function analyseSubjectivity(subjectivity) {
                        if (subjectivity > 0.5) {
                        return \'Subjective\';
                        } else if (subjectivity >= 0.3) {
                        return \'Neutral\';
                        } else {
                        return \'Not Subjective\';
                        }
                    }

                    setTimeout(function() {
                        var p = document.getElementById(\'polarity\');
                        var s = document.getElementById(\'subjectivity\');
                        p.textContent = analysePolarity(p.textContent) + \' (\'+p.textContent+\')\';
                        s.textContent = analyseSubjectivity(s.textContent) + \' (\'+s.textContent+\')\';
                    }, 10000);
                </script>';
            ?>

            <?php
                $text = '';
                $favTweets = [];
                foreach ($_SESSION['favorite'] as $key => $value) {
                    foreach ($tweets as $k => $t) {
                        if ($t['id'] == $value) {
                            array_push($favTweets, $t);
                        }
                    }
                }
                foreach ($favTweets as $key => $value) {
                    if ($value === end($favTweets)) {
                        $text = $text . '"'.str_replace('"', '', preg_replace('/\s+/', ' ', $value['text'])).'"';
                    }
                    else {
                        $text = $text . '"'.str_replace('"', '', preg_replace('/\s+/', ' ', $value['text'])).'",';
                    }
                }
                echo '<py-script>
                    from textblob import Blobber
                    from textblob_fr import PatternTagger, PatternAnalyzer
                    import matplotlib.pyplot as plt
                    import statistics

                    tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
                    sentences = ['.$text.']

                    polarity = []
                    subjectivity = []

                    for i in range(len(sentences)):
                        blob = tb(sentences[i])
                        polarity.append(blob.sentiment[0])
                        subjectivity.append(blob.sentiment[1])

                    # Calculer la moyenne
                    moyPolarity = statistics.mean(polarity)
                    moySubjectivity = statistics.mean(subjectivity)

                    p = Element("polarity")
                    p.write(moyPolarity)
                    s = Element("subjectivity")
                    s.write(moySubjectivity)

                    # Changement du fond de couleur de la figure
                    plt.style.use(\'dark_background\')
                    # Crée une figure et un axe
                    fig, ax = plt.subplots()
                    # Limite en ordonnées de -1 à 1
                    plt.ylim(-1, 1)
                    # Ajout des légendes sur les axes
                    plt.xlabel(\'Number of Tweets\')
                    plt.ylabel(\'Polaritity\')
                    plt.plot(range(len(polarity)), polarity, color=\'red\')
                    polarity_all_tweets = Element("polarity_all_tweets")
                    polarity_all_tweets.write(fig)

                    # Changement du fond de couleur de la figure
                    plt.style.use(\'dark_background\')
                    # Crée une figure et un axe
                    fig, ax = plt.subplots()
                    # Limite en ordonnées de 0 à 1
                    plt.ylim(0, 1)
                    # Ajout des légendes sur les axes
                    plt.xlabel(\'Number of Tweets\')
                    plt.ylabel(\'Subjectivity\')
                    plt.plot(range(len(subjectivity)), subjectivity, color=\'green\')
                    subjectivity_all_tweets = Element("subjectivity_all_tweets")
                    subjectivity_all_tweets.write(fig)
                </py-script>';
            ?>
        <?php endif; ?>

    <?php else: ?>
        <section class="pyscript">
            <h3 class="text-center">Polarity of all Tweets</h3>
            <div id="polarity_all_tweets">...</div>
        </section>
        <section class="pyscript">
            <h3 class="text-center">Subjectivity of all Tweets</h3>
            <div id="subjectivity_all_tweets">...</div>
        </section>

        <div class="info-analysis">
            <div class="card bg-dark text-white" style="width: 70em;">
                <div class="card-body">
                    <h5 class="card-title mb-5">Summary of Information of all Tweets</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text"><img src="images/polarity.svg" alt="The average of Polarity"> The average of Polarity:
                                <p>
                                    <strong class="card-text">Sentiment analysis is
                                    <span id="polarity">...</span></strong>
                                </p>
                            </p>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="col-md-6">
                            <p class="card-text"><img src="images/subjectivity.svg" alt="The average of Subjectivity"> The average of Subjectivity:
                                <p>
                                    <strong class="card-text">Sentiment analysis is
                                    <span id="subjectivity">...</span></strong></p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            echo '<script defer>
                function analysePolarity(polarity) {
                    if (polarity < 0) {
                    return \'Negative\';
                    } else if (polarity > 0) {
                    return \'Positive\';
                    } else {
                    return \'Neutral\';
                    }
                }

                function analyseSubjectivity(subjectivity) {
                    if (subjectivity > 0.5) {
                    return \'Subjective\';
                    } else if (subjectivity >= 0.3) {
                    return \'Neutral\';
                    } else {
                    return \'Not Subjective\';
                    }
                }

                setTimeout(function() {
                    var p = document.getElementById(\'polarity\');
                    var s = document.getElementById(\'subjectivity\');
                    p.textContent = analysePolarity(p.textContent) + \' (\'+p.textContent+\')\';
                    s.textContent = analyseSubjectivity(s.textContent) + \' (\'+s.textContent+\')\';
                }, 10000);
            </script>';
        ?>

        <?php
            $text = '';
            foreach ($tweets as $key => $value) {
                if ($value === end($tweets)) {
                    $text = $text . '"'.str_replace('"', '', preg_replace('/\s+/', ' ', $value['text'])).'"';
                }
                else {
                    $text = $text . '"'.str_replace('"', '', preg_replace('/\s+/', ' ', $value['text'])).'",';
                }
            }
            echo '<py-script>
                    from textblob import Blobber
                    from textblob_fr import PatternTagger, PatternAnalyzer
                    import matplotlib.pyplot as plt
                    import statistics

                    tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
                    sentences = ['.$text.']

                    polarity = []
                    subjectivity = []

                    for i in range(len(sentences)):
                        blob = tb(sentences[i])
                        polarity.append(blob.sentiment[0])
                        subjectivity.append(blob.sentiment[1])

                    # Calculer la moyenne
                    moyPolarity = statistics.mean(polarity)
                    moySubjectivity = statistics.mean(subjectivity)

                    p = Element("polarity")
                    p.write(moyPolarity)
                    s = Element("subjectivity")
                    s.write(moySubjectivity)

                    # Changement du fond de couleur de la figure
                    plt.style.use(\'dark_background\')
                    # Crée une figure et un axe
                    fig, ax = plt.subplots()
                    # Limite en ordonnées de -1 à 1
                    plt.ylim(-1, 1)
                    # Ajout des légendes sur les axes
                    plt.xlabel(\'Number of Tweets\')
                    plt.ylabel(\'Polaritity\')
                    plt.plot(range(len(polarity)), polarity, color=\'red\')
                    polarity_all_tweets = Element("polarity_all_tweets")
                    polarity_all_tweets.write(fig)

                    # Changement du fond de couleur de la figure
                    plt.style.use(\'dark_background\')
                    # Crée une figure et un axe
                    fig, ax = plt.subplots()
                    # Limite en ordonnées de 0 à 1
                    plt.ylim(0, 1)
                    # Ajout des légendes sur les axes
                    plt.xlabel(\'Number of Tweets\')
                    plt.ylabel(\'Subjectivity\')
                    plt.plot(range(len(subjectivity)), subjectivity, color=\'green\')
                    subjectivity_all_tweets = Element("subjectivity_all_tweets")
                    subjectivity_all_tweets.write(fig)
                </py-script>';
        ?>
    <?php endif; ?>
    
    <!-- Script JS gérant la recherche des tweets avec un mot clé -->
    <?php
        echo '<script defer>
            var btnSearch = document.getElementById(\'btnSearch\');
            var search = document.getElementById(\'search\');

            btnSearch.addEventListener(\'click\', function() {
                if (search.value != \'\') {
                    var value = search.value;
                    value = value.toLowerCase();
                    value = value.replace(/ /g,\'\');
                    window.location.href = \'index.php?search=\' + value;
                }
            });

            search.addEventListener("keydown", function(event) {
                if(event.keyCode === 13) {
                    if (search.value != \'\') {
                        var value = search.value;
                        value = value.toLowerCase();
                        value = value.replace(/ /g,\'\');
                        window.location.href = \'index.php?search=\' + value;
                    }
                }
            });
        </script>';
    ?>

    <div class="top">
        <img src="images/top.svg" id="top" height="60" alt="Scroll to Top" alt="Scroll to Top">
    </div>

    <footer class="bg-dark text-light text-center py-3 mt-3">
        <div class="container">
            <p><a href="index.php"><img src="images/twitter_white.svg" height="30" alt="Twitter" title="Twitter"></a> <a href="index.php">Twitter Project</a> - Sentiment Analysis of Tweets</p>
            <p>Designed and Developed by <a href="mailto:antho.coulon22@gmail.com">Anthony Coulon</a> & <a href="mailto:titouan.comtet@gmail.com">Titouan Comtet</a></p>
            <p>Hosted by <a href="https://infinityfree.net/" target="_blank">InfinityFree <img src="images/infinityfree.png" height="50" alt="InfinityFree" title="InfinityFree"></a></p>
            <p>Tweets retrieve on <a href="https://twitter.com" target="_blank">Twitter</a> <a href="https://twitter.com" target="_blank"><img src="images/twitter_white.svg" height="30" alt="Twitter" title="Twitter"></a></p>
        </div>
    </footer>

    <script src="scripts/managerBtnTop.js" defer></script>
</body>

</html>