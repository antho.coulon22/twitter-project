
# Application d'analyse de sentiment de tweets - Twitter Project

Cette application web utilise la bibliothèque TextBlob et son extension TextBlob_fr pour l'analyse de sentiments de tweets, concernant la réforme des retraites en France, en français récupérés depuis Twitter.

## Sommaire

1.  [Fonctionnalités](#1-fonctionnalités)
2.  [Prérequis pour notre projet](#2-prérequis-pour-notre-projet)
3.  [Technologies utilisées](#3-technologies-utilisées)
4.  [Documentation développeur](#4-documentation-développeur)
5.  [Lien vers l'application web](#5-lien-vers-lapplication-web)
6.  [Coordonnées des développeurs](#6-coordonnées-des-développeurs)

## 1. Fonctionnalités

-   Analyse de sentiments : l'application analyse les sentiments des tweets récupérés depuis Twitter en utilisant TextBlob et TextBlob_fr.

-   Tri des tweets : l'utilisateur peut trier les tweets par ordre croissant ou décroissant des retweets, par recherche de mots clés, ou par nombre de favoris.

-   Gestion des favoris : l'utilisateur peut ajouter des tweets à ses favoris et les visualiser dans une liste dédiée. Il a aussi l'option de les supprimer.

-   Informations sur les tweets : l'application fournit des informations sur tous les tweets récupérés dont le nombre de tweets et la date de récupération, et pour chaque tweet le texte du tweet, le nombre de retweets, l'identifiant du tweet et de l'utilisateur ayant tweeté.

## 2. Prérequis pour notre projet

Pour ce projet, nous avons fais appel aux bibliothèques TextBlob et TextBlob_fr installées dans notre environnement. Nous avons également créer un compte Twitter Développeur afin de générer des clés API Twitter pour accéder aux tweets et les récupérer.

## 3. Technologies utilisées

Le projet a été développé en utilisant les technologies suivantes :

- HTML/CSS (SCSS avec SASS) : Langage de balisage et langage de style pour la création de la structure et de la mise en page de l'application web.

- Bootstrap : Framework de développement web front-end utilisé pour créer une interface utilisateur réactive et mobile-friendly.

- PHP : Langage de programmation côté serveur pour le traitement des données et la génération de contenu dynamique.

- JavaScript : Langage de programmation côté client pour la manipulation du DOM et la création d'effets interactifs.

- PyScript (Python) : Framework pour utiliser le langage Python dans un navigateur (intégré dans le HTML).

- TextBlob & TextBlob_fr : Bibliothèque Python pour l'analyse de sentiment et la traduction de texte en français.

Ces technologies ont été choisies pour leur compatibilité, leur facilité d'utilisation, leur fiabilité pour répondre aux besoins du projet. 
Par ailleurs, c'est aussi un défi d'utiliser le récent framework PyScript qui nous a donné envie de l'essayer sur ce projet !

## 4. Documentation développeur

1.  Ouvrir le terminal et se placer dans le dossier dans lequel vous souhaitez cloner le projet.

2.  Cloner le projet en exécutant la commande suivante dans le terminal : `git clone https://gitlab.com/antho.coulon22/twitter-project.git`

3.  Se déplacer dans le dossier twitter-project à l'aide de la commande suivante : `cd twitter-project`

4.  Lancer le serveur local en exécutant la commande suivante : `php -S localhost:8008`

5.  Accédez à l'application en ouvrant un navigateur Web et en accédant à l'URL : [http://localhost:8008](http://localhost:8008).

Vous êtes maintenant prêt à utiliser l'application Twitter Project en tant que développeur !

## 5. Lien vers l'application web

Vous pouvez accéder à l'application web à l'adresse suivante : [Twitter Project](http://twitter-project.great-site.net/).
Assurez-vous d'avoir une connexion Internet stable pour une utilisation optimale de l'application.

Veuillez noter que le temps de chargement de la page peut être assez long, car l'application utilise le framework PyScript pour l'analyse des sentiments des tweets et l'affichage des graphiques. Ce temps de chargement est long pendant la toute première analyse sur le site et est réduit par la suite. Soyez patient et attendez que la page se charge complètement avant d'utiliser l'application, merci.

Lien : [http://twitter-project.great-site.net/](http://twitter-project.great-site.net/)

## 6. Coordonnées des développeurs

L'application a été conçue et développée par [Anthony Coulon](mailto:antho.coulon22@gmail.com) & [Titouan Comtet](mailto:titouan.comtet@gmail.com)