let lastScrollTop = 0;
const btnScrollTop = document.querySelector('#top');

window.addEventListener('scroll', function () {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    const clientHeight = document.documentElement.clientHeight;

    if (scrollTop > clientHeight / 10) {
        btnScrollTop.style.display = 'block';
    }
    else {
        btnScrollTop.style.display = 'none';
    }
    lastScrollTop = scrollTop;
});

btnScrollTop.addEventListener('click', function () {
    window.scrollTo({ top: 0, behavior: 'smooth' });
});