var checkbox = document.getElementById('myfavorite');
checkbox.addEventListener('click', function () {
    if (this.checked) {
        window.location.href = '?myfavorite=';
    } else {
        if (window.location.href.includes('myfavorite=&')) {
            window.location.href = window.location.href.replace('myfavorite=&', '');
        }
        else {
            window.location.href = window.location.href.replace('?myfavorite=', '');
        }
    }
});