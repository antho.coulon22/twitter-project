<?php
    session_start();

    // Vérifier le tri des retweets
    $filtre = '?';
    $search = '';
    $myfavorite = '';
    $page = '';
    if (isset($_GET['asc'])) {
        $filtre = '?asc=&';
    }
    else if (isset($_GET['desc'])) {
        $filtre = '?desc=&';
    }
    if (isset($_GET['search']) && !empty($_GET['search'])) {
        $search = 'search=' . $_GET['search'] . '&';
    }
    if (isset($_GET['myfavorite'])) {
        $myfavorite = 'myfavorite=&';
    }
    if (isset($_GET['page'])) {
        $page = 'page='.$_GET['page'].'&';
    }

    if (!isset($_SESSION['favorite'])) {
        $_SESSION['favorite'] = [];
    }

    if (isset($_GET['favorite'])) {
        if (!in_array($_GET['favorite'], $_SESSION['favorite'])) {
            array_push($_SESSION['favorite'], $_GET['favorite']);
            $_SESSION['date_favorite'] = DATE('d/m/Y');
        }
        else {
            $id_fav_remove = array_search($_GET['favorite'], $_SESSION['favorite']);
            unset($_SESSION['favorite'][$id_fav_remove]);
        }
        header('Location: index.php'.$filtre.$search.$myfavorite.$page.'#' . $_GET['favorite']);
    }
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://abs.twimg.com/favicons/twitter.2.ico" type="image/x-icon">
    <title>List of tweets</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
        // VARIABLES

        // Récupérer le contenu du fichier tweets.json
        $json_str = file_get_contents('tweets.json');

        // Décoder le contenu JSON en UTF-8 pour avoir le même encodage avec l'affichage
        $json_str_utf8 = utf8_decode($json_str);
        // Décoder le contenu JSON en tant que tableau associatif
        $tweets = json_decode($json_str_utf8, true)['data'];

        if (isset($_GET['search']) && !empty($_GET['search'])) {
            // Si la recherche est active alors on trie les tweets avec la valeur recherchée
            function search_sort($search_value, $tweets) {
                $new_tweets = array();
                foreach ($tweets as $tweet) {
                    if (strpos(strtolower(preg_replace('/\s+/', '', $tweet['text'])), $search_value) !== false) {
                        // Si la valeur recherchée est trouvée dans le texte du tweet, on l'ajoute au tableau de tweets triés
                        $new_tweets[] = $tweet;
                    }
                }
                return $new_tweets;
            }
            $search_value = $_GET['search'];
            $tweets = search_sort($search_value, $tweets);
        }

        if (isset($_GET['myfavorite'])) {
            $newtweets = [];
            if (isset($_SESSION['favorite'])) {
                foreach ($_SESSION['favorite'] as $key => $value) {
                    foreach ($tweets as $k => $v) {
                        if ($v['id'] === $value) {
                            array_push($newtweets, $v);
                        }
                    }
                }
            }
            $tweets = $newtweets;
        }

        if (isset($_GET['asc'])) {
            // Si tri ascendant alors on tri le tableau de tweets par ordre croissant
            function retweets_usort($retweets_a, $retweets_b) {
                return $retweets_a['public_metrics']['retweet_count'] - $retweets_b['public_metrics']['retweet_count'];
            }
            usort($tweets, "retweets_usort");
        }
        else if (isset($_GET['desc'])) {
            // Si tri descendant alors on tri le tableau de tweets par ordre décroissant
            function retweets_ursort($retweets_a, $retweets_b) {
                return $retweets_b['public_metrics']['retweet_count'] - $retweets_a['public_metrics']['retweet_count'];
            }
            usort($tweets, "retweets_ursort");
        }

        // Déterminer le nombre de tweets
        $num_tweets = count($tweets);

        // Déterminer le numéro de page à afficher
        $current_page = isset($_GET['page']) ? $_GET['page'] : 1;

        // Déterminer le nombre X de tweets par page
        $tweets_per_page = isset($_GET['tweets_per_page']) ? $_GET['tweets_per_page'] : 10;
        if ($tweets_per_page != 10) {
            $tpp = '&tweets_per_page=' . $tweets_per_page;
        }
        else {
            $tpp = '';
        }

        // Déterminer la position du premier tweet de la page courante
        $first_tweet = ($current_page - 1) * $tweets_per_page;

        // Récupérer les X tweets correspondants à la page courante
        $tweets_page = array_slice($tweets, $first_tweet, $tweets_per_page);
    ?>

    <!-- Menu de la page -->
    <nav class="navbar navbar-expand-lg bg-dark fixed-top navbar-dark menu">
        <div style="margin-left: 1em;">
            <a class="navbar-brand" href="index.php"><img src="images/twitter_white.svg" alt="Twitter Project" height="40"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="margin-right: 1em;">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="menu__item"><a href="index.php" class="active"><img src="images/explore.svg" alt="Explore" height="30"> Explore</a></li>
                <li class="menu__item"><a href="sentiment-analysis.php"><img src="images/add_chart.svg" alt="Sentiment analysis" height="30"> Sentiment analysis</a></li>
                <li class="menu__item"><a href="additional-infos.php"><img src="images/infos.svg" alt="Additional info" height="30"> Additional info</a></li>
                <li class="menu__item"><a href="my-favorite-tweets.php"><img src="images/favorite-white.svg" alt="My favorite Tweets" height="30"> My favorite Tweets</a></li>
                <li class="menu__item"><a href="https://twitter.com" target="_blank"><img src="images/logo-white.png" alt="Twitter" height="30"> Official Twitter</a></li>
                <li class="menu__search" style="margin-left: 5em;margin-right: 1em;">
                    <input type="search" name="search" id="search" class="form-control" <?php if(isset($_GET['search'])) {
                        echo 'value="' . $_GET['search'] . '"';
                    }?> placeholder="Search keyword..." maxlength="50">
                    <button class="btn btn-light" id="btnSearch"><img src="images/search.svg" height="25" alt="Search" title="Search"></button>
                </li>
            </ul>
        </div>
    </nav>

    <!-- Filtres de la page -->
    <div class="filter">
        <div>
            <label for="retweets" class="filter__label">Number of Retweets</label>
            <select class="filter__select" id="retweets" name="retweets">
                <option value="default">By default</option>
                <option value="asc" <?php (isset($_GET['asc'])) ? print_r("selected") : '' ?>>Ascending</option>
                <option value="desc" <?php (isset($_GET['desc'])) ? print_r("selected") : '' ?>>Descending</option>
            </select>

            <div class="form-check filter__check">
                <input class="form-check-input" type="checkbox" name="myfavorite" id="myfavorite" <?php if (isset($_GET['myfavorite'])) {
                    echo 'checked="true"';
                }?>>
                <label class="form-check-label filter__label" for="myfavorite">
                    My favorite Tweets
                </label>
            </div>
        </div>

        <div>
            <label for="keyword" class="filter__label">
                <?php
                    if(isset($_GET['search']) && !empty($_GET['search']))
                        echo 'Search word: '.$_GET['search'].'<span class="badge bg-danger" title="Remove search word" style="margin-left: 0.5em; cursor: pointer;" onclick="window.location.href = window.location.href.replace(\'search='.$_GET['search'].'&\', \'\');window.location.href = window.location.href.replace(\'search='.$_GET['search'].'\', \'\');">&#10005;</span></span>
                        <br>
                        Tweet(s) found: ' . $num_tweets;
                    else
                        echo '';
                ?>
            </label>
        </div>

        <!-- Formulaire pour choisir le nombre de tweets par page -->
        <form method="get">
            <div class="form-group">
                <label for="tweets_per_page" class="filter__label">Tweets per page</label>
                <select class="form-control" id="tweets_per_page" name="tweets_per_page" onchange="this.form.submit()">
                    <option value="5" <?php if ($tweets_per_page == 5) echo 'selected'; ?>>5</option>
                    <option value="10" <?php if ($tweets_per_page == 10) echo 'selected'; ?>>10</option>
                    <option value="20" <?php if ($tweets_per_page == 20) echo 'selected'; ?>>20</option>
                    <option value="50" <?php if ($tweets_per_page == 50) echo 'selected'; ?>>50</option>
                </select>
            </div>
        </form>
    </div>

    <!-- Titre de la page -->
    <h1 class="title">List of tweets about retirement!</h1>

    <!-- Zone des tweets -->
    <div class="tweets">
        <?php
            if (count($tweets) === 0) {
                $msgFav = '';
                if (isset($_GET['myfavorite'])) {
                    $msgFav = 'favorite';
                }
                echo '<p style="font-size: 1.2em; color: #e86600ff;"><img src="images/warning.svg" height="30" alt="No tweets found!" style="margin-right: 0.2em">No '.$msgFav.' Tweets found!</p>';
            }
            function formatURL($text) {
                $urlRegex = '/(https?:\/\/[^\s]+)/';
                return preg_replace($urlRegex, '<a href="$0" target="_blank">$0</a>', $text);
            }
            function formatMention($text) {
                $mentionRegex = '/@([A-Za-z0-9_]+)/';
                return preg_replace($mentionRegex, '<a href="https://twitter.com/$1" target="_blank" style="text-decoration: none;">$0</a>', $text);
            }
            // Parcourir les tweets et afficher leurs informations
            foreach ($tweets_page as $tweet) {
                $img_fav = 'prefavorite.svg';
                $info_fav = 'Add to favorites';
                if (in_array($tweet['id'], $_SESSION['favorite'])) {
                    $img_fav = 'favorite.svg';
                    $info_fav = 'Remove to favorites';
                }
                echo '<div class="tweets__tweet" id="'.$tweet['id'].'">           
                        <div class="tweet-header">
                            <img src="images/twitter.svg" alt="Café">
                            <h3>Utilisateur '.$tweet['author_id'].'</h3>
                        </div>
                        <p class="tweet-content">'.formatMention(formatURL($tweet['text'])).'</p>
                        <p class="tweet-retweets" title="'.$tweet['public_metrics']['retweet_count'].' Retweets">
                            <img src="images/retweet.svg" alt="Retweets" height="30">
                            <span>'.$tweet['public_metrics']['retweet_count'].'</span>
                        </p>
                        <div class="tweet-footer">
                            <p>06/02/2023</p>
                            <div>
                                <img src="images/'.$img_fav.'" alt="'.$info_fav.'" title="'.$info_fav.'" height="40" class="tweet-favorite" id="favorite'.$tweet['id'].'" onclick="window.location.href = \'index.php'.$filtre.$search.$myfavorite.$page.'favorite='.$tweet['id'].'\'">
                                <img src="images/add_chart.svg" alt="Launch Sentiment analysis of this Tweet" title="Launch Sentiment analysis of this Tweet" height="40" class="tweet-launch" onclick="window.location.href = \'sentiment-analysis.php?id='.$tweet['id'].'\'">
                            </div>
                        </div>
                    </div>';
            }

            // Affichage de la pagination
            $totalPages = ceil($num_tweets / $tweets_per_page);

            if ($totalPages > 1) {
                // Création de la pagination
                echo '<div class="pagination">';
                if ($current_page > 1) {
                    // Affichage de la redirection vers la première page
                    echo '<a href="'.$filtre.$search.$myfavorite.'page=1'.$tpp.'">&lt;&lt;</a>';
                }
                if (max(1, $current_page - 3) > 1) {
                    // Affichage des points de suspension si la pagination de la première page n'est plus visible
                    echo '&nbsp;...&nbsp;';
                }
                else {
                    // Affichage d'un espace après le numéro uno si pas de points de suspension
                    echo '&nbsp;';
                    if ($current_page != 1) {
                        echo '|&nbsp;';
                    }
                }
                for ($i = max(1, $current_page - 3); $i <= min($current_page + 3, $totalPages); $i++) {
                    if ($i == $current_page) {
                        // Affichage de la page courante
                        echo '<span class="current">' . $i . '</span>';
                        if ($current_page != $totalPages) {
                            // Séparation si on est pas sur la dernière page
                            echo '&nbsp;|&nbsp;';
                        }
                    } else {
                        // Affichage des numéros des liens de la pagination
                        echo '<a href="'.$filtre.$search.$myfavorite.'page=' . $i . $tpp . '">' . $i . '</a>&nbsp;|&nbsp;';
                    }
                }
                if (min($current_page + 3, $totalPages) < $totalPages) {
                    // Affichage des points de suspension si la pagination de la dernière page n'est plus visible
                    echo '&nbsp;...&nbsp;';
                }
                if ($current_page < $totalPages) {
                    // Affichage de la redirection vers la dernière page
                    echo '<a href="'.$filtre.$search.$myfavorite.'page=' . ($totalPages) . $tpp . '">&gt;&gt;</a>';
                }
                echo '</div>';
            }
        ?>
    </div>

    <!-- Script JS gérant le changement du filtre des retweets avec redirection -->
    <?php
        echo '<script defer>
            var retweets = document.getElementById(\'retweets\');
            retweets.addEventListener(\'change\', function(event) {
                switch (event.target.value) {
                    case \'asc\':
                        window.location.href = \'?'.$search.$myfavorite.'asc='.$tpp.'\';
                        break;
                        
                    case \'desc\': window.location.href = \'?'.$search.$myfavorite.'desc='.$tpp.'\';
                        break;

                    default: window.location.href = \'?'.$search . $myfavorite . $tpp .'\';
                        break;
                }
            });
        </script>';
    ?>

    <!-- Script JS gérant la recherche des tweets avec un mot clé -->
    <?php
        echo '<script defer>
            var btnSearch = document.getElementById(\'btnSearch\');
            var search = document.getElementById(\'search\');

            btnSearch.addEventListener(\'click\', function() {
                if (search.value != \'\') {
                    var value = search.value;
                    value = value.toLowerCase();
                    value = value.replace(/ /g,\'\');
                    window.location.href = \''.$filtre .$page. $myfavorite .'search=\' + value + \''.$tpp.'\';
                }
                else {
                    window.location.href = \'index.php\';
                }
            });

            search.addEventListener("keydown", function(event) {
                if(event.keyCode === 13) {
                    if (search.value != \'\') {
                        var value = search.value;
                        value = value.toLowerCase();
                        value = value.replace(/ /g,\'\');
                        window.location.href = \''.$filtre .$page. $myfavorite .'search=\' + value + \''.$tpp.'\';
                    }
                    else {
                        window.location.href = \'index.php\';
                    }
                }
            });

        </script>';
    ?>

    <div class="top">
        <img src="images/top.svg" id="top" height="60" alt="Scroll to Top" alt="Scroll to Top">
        <img src="images/add_chart.svg" alt="Launch Sentiment analysis of all Tweets" height="60" title="Launch Sentiment analysis of all Tweets" class="launch" onclick="window.location.href = 'sentiment-analysis.php'">
    </div>
        
    <footer class="bg-dark text-light text-center py-3 mt-3">
        <div class="container">
            <p><a href="index.php"><img src="images/twitter_white.svg" height="30" alt="Twitter" title="Twitter"></a> <a href="index.php">Twitter Project</a> - Sentiment Analysis of Tweets</p>
            <p>Designed and Developed by <a href="mailto:antho.coulon22@gmail.com">Anthony Coulon</a> & <a href="mailto:titouan.comtet@gmail.com">Titouan Comtet</a></p>
            <p>Hosted by <a href="https://infinityfree.net/" target="_blank">InfinityFree <img src="images/infinityfree.png" height="50" alt="InfinityFree" title="InfinityFree"></a></p>
            <p>Tweets retrieve on <a href="https://twitter.com" target="_blank">Twitter</a> <a href="https://twitter.com" target="_blank"><img src="images/twitter_white.svg" height="30" alt="Twitter" title="Twitter"></a></p>
        </div>
    </footer>

    <script src="scripts/managerBtnTop.js" defer></script>
    <script src="scripts/managerCheckboxFavorite.js" defer></script>
</body>

</html>